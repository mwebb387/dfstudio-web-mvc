﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DFStudioData.Models
{
    [Table(Name = "Lev")]
    public class Lev
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        public int ID { get; set; }

        [Column]
        public int ProjectId { get; set; }

        [Column]
        public string Name { get; set; }

        [Column]
        public string Level { get; set; }

        [Column]
        public string Pallate { get; set; }
    }
}
