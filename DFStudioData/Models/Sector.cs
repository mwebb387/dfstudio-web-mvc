﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DFStudioData.Models
{
    [Table(Name = "Sector")]
    public class Sector
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        public int ID { get; set; }

        [Column]
        public int LevId { get; set; }

        [Column]
        public string Name { get; set; }

        [Column]
        public double CeilingAlt { get; set; }

        [Column]
        public double FloorAlt { get; set; }

        public List<Wall> Walls { get; set; } = new List<Wall>();
        public List<Vertex> Vertices { get; set; } = new List<Vertex>();
    }
}
