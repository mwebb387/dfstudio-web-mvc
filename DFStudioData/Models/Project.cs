﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DFStudioData.Models
{
    [Table(Name = "Project")]
    public class Project
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        public int ID { get; set; }

        [Column]
        public string Name { get; set; }

        [Column]
        public string Author { get; set; }

        [Column]
        public string Description { get; set; }
    }
}
