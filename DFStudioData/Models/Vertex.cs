﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DFStudioData.Models
{
    [Table(Name = "Vertex")]
    public class Vertex
    {
        [Column]
        public int ID { get; set; }

        [Column]
        public int SectorId { get; set; }

        [Column]
        public double X { get; set; }

        [Column]
        public double Y { get; set; }
    }
}
