﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DFStudioData.Models
{
    [Table(Name = "Wall")]
    public class Wall
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        public int ID { get; set; }

        [Column]
        public int SectorId { get; set; }

        [Column]
        public string MidTx { get; set; }
    }
}
