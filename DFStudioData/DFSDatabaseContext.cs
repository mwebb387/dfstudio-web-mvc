﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DFStudioData
{
    public class DFSDatabaseContext : DbContext
    {
        public DFSDatabaseContext(string dbFilename)
            : base(new SQLiteConnection() {
                ConnectionString = new SQLiteConnectionStringBuilder() {
                    DataSource = dbFilename,
                    ForeignKeys = true
                }.ConnectionString,
            }, true)
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Models.Project> Project { get; set; }
        public DbSet<Models.Lev> Lev { get; set; }
        public DbSet<Models.Sector> Sector { get; set; }
        public DbSet<Models.Wall> Wall { get; set; }
        public DbSet<Models.Vertex> Vertex { get; set; }
    }
}
