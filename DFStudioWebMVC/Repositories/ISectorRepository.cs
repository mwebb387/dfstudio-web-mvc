﻿using DFStudioData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DFStudioWebMVC.Repositories
{
    public interface ISectorRepository
    {
        IEnumerable<Sector> GetAll();
        IEnumerable<Sector> GetAllByLevId(int levId);
        Sector GetById(int id);
        void Add(Sector sec);
        void Update(Sector sec);
        void Delete(Sector sec);
    }
}
