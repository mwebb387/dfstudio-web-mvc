﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DFStudioData.Models;

namespace DFStudioWebMVC.Repositories
{
    public class DFSectorRepository : ISectorRepository
    {
        public void Add(Sector sec)
        {
            Business.DFSDatabase.Context.Sector.Add(sec);

            foreach (Wall wall in sec.Walls)
                Business.DFSDatabase.Context.Wall.Add(wall);

            foreach (Vertex vx in sec.Vertices)
                Business.DFSDatabase.Context.Vertex.Add(vx);

            Business.DFSDatabase.Context.SaveChanges();
        }

        public void Delete(Sector sec)
        {
            Business.DFSDatabase.Context.Sector.Remove(sec);

            //NOTE: DB should cascade the delete to Walls and Vertices
            Business.DFSDatabase.Context.SaveChanges();
        }

        public IEnumerable<Sector> GetAll()
        {
            return Business.DFSDatabase.Context.Sector
                .ToList()
                .Select(sec => BuildSector(sec));
        }

        public IEnumerable<Sector> GetAllByLevId(int levId)
        {
            return Business.DFSDatabase.Context.Sector
                .Where(sec => sec.LevId == levId)
                .ToList()
                .Select(sec => BuildSector(sec));
        }

        public Sector GetById(int id)
        {
            Sector sector = Business.DFSDatabase.Context.Sector
                .FirstOrDefault(sec => sec.ID == id);

            return BuildSector(sector);
        }

        public void Update(Sector sec)
        {
            //TODO: Update...

            //Sector replace = GetById(sec.ID);
            //replace = sec;
            //Business.DFSDatabase.Context.SaveChanges();
        }

        private Sector BuildSector(Sector sec)
        {
            sec.Walls = Business.DFSDatabase.Context.Wall
                .Where(wall => wall.SectorId == sec.ID)
                .ToList();
            sec.Vertices = Business.DFSDatabase.Context.Vertex
                .Where(vx => vx.SectorId == sec.ID)
                .ToList();

            return sec;
        }
    }
}