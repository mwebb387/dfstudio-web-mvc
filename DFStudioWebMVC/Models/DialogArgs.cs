﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFStudioWebMVC.Models
{
    public class DialogArgs
    {
        public string Title { get; set; }
        public string Message { get; set; }
    }
}