﻿using DFStudioData.Models;
using DFStudioWebMVC.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DFStudioWebMVC.Controllers
{
    public class SectorsController : ApiController
    {
        ISectorRepository repo;

        public SectorsController(ISectorRepository sectorRepository)
        {
            repo = sectorRepository;
        }

        // GET: api/Sectors/5
        public Sector Get(int id)
        {
            return repo.GetById(id);
        }

        // GET: api/Sectors/ByLevelId/5
        [Route("api/Sectors/ByLevelId/{levelId}")]
        public IEnumerable<Sector> GetByLevelId(int levelId)
        {
            return repo.GetAllByLevId(levelId);
        }

        // POST: api/Sectors
        public void Post([FromBody]Sector value)
        {
            repo.Add(value);
        }

        // PUT: api/Sectors/5
        public void Put(int id, [FromBody]Sector value)
        {
            value.ID = id;

            //TODO: Make sure this works...
            repo.Update(value);
        }

        // DELETE: api/Sectors/5
        public void Delete(int id)
        {
            repo.Delete(repo.GetById(id));
        }
    }
}
