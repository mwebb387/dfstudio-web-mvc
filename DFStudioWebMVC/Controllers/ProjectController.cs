﻿using DFStudioData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DFStudioWebMVC.Controllers
{
    public class ProjectController : Controller
    {
        // GET: Project
        public ActionResult Index()
        {
            return View(Business.DFSDatabase.Context.Project.ToList());
        }

        public ActionResult Details(int id)
        {
            var project = Business.DFSDatabase.Context.Project.Where(p => p.ID == id);

            return RedirectToAction("Index"); //TODO: Update
        }

        public ActionResult NewProject()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewProject(Project proj)
        {
            //TODO: Create new project...

            return RedirectToAction("Index");
        }
    }
}