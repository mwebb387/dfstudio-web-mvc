﻿using DFStudioData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DFStudioWebMVC.Controllers
{
    public class LevelController : Controller
    {
        // GET: Level
        public ActionResult Index(int projectId)
        {
            var levels = Business.DFSDatabase.Context.Lev.Where(l => l.ProjectId == projectId);
            return View(levels);
        }

        public ActionResult Edit(int id)
        {
            var level = Business.DFSDatabase.Context.Lev.First(lev => lev.ID == id);
            return View(level);
        }

        public ActionResult NewLevel()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewLevel(Lev level)
        {
            //TODO: Create new level...

            return RedirectToAction("Index");
        }
    }
}