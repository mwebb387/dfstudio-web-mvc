var app = app || {};

(function() {
    /* Defines the Sector draw mode for the lev-canvas */

    var EditModeSector = function () {
        this.name = 'Sector';
        this.lev = null;
        this.sector = null;
    }

    EditModeSector.prototype.mousedown = function(levCvs, pt) {
        console.log('Down: ' + pt.X + ', ' + pt.Y);

        for (var s = 0; s < this.lev.sectors.length; ++s) {
            var sector = this.lev.sectors.at(s);

            if (levCvs.hitTestPolygon(sector.get('Vertices'), pt))
            {
                if (sector.get('Selected')) {
                    this.sector = sector;
                    return;
                } else {
                    var selected = this.lev.sectors.find(function (sec) {
                        return sec.get('Selected');
                    });
                    if (selected)
                        selected.set('Selected', false);
                    sector.set('Selected', true);
                }
            }
        }
    };

    EditModeSector.prototype.mouseup = function(levCvs, pt) {
        console.log('Up: ' + pt.X + ', ' + pt.Y);

        this.sector = null;
    };

    EditModeSector.prototype.mousemove = function(levCvs, ptNew, ptPrev) {
        if (this.sector) {
            var dx = ptNew.X - ptPrev.X;
            var dy = ptNew.Y - ptPrev.Y;
            this.sector.translate(dx, dy);
        } else {
            for (var s = 0; s < this.lev.sectors.length; ++s) {
                var sector = this.lev.sectors.at(s);

                if (sector.get('Selected') && levCvs.hitTestPolygon(sector.get('Vertices'), ptNew))
                {
                    levCvs.setCursor('move');
                } else {
                    levCvs.setCursor('default');
                }
            }
        }
    };

    EditModeSector.prototype.draw = function(levCvs) {

        for (var s = 0; s < this.lev.sectors.length; ++s) {
            var sector = this.lev.sectors.at(s);

            var strokeStyle = 'white';
            if (sector.get('Selected')) {
                strokeStyle = 'red';
            }

            levCvs.drawPolygon(sector.get('Vertices'), 2, strokeStyle);
        }
    };

    app.EditModeSector = EditModeSector;
})();
