( function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define( [
			"jquery",
			"jquery-ui/widget"
		], factory );
	} else {
		// Browser globals
		factory( jQuery );
	}
}( function( $ ) {
    $.widget("ui.levcanvas", {

        //
        // private properties
        //
        _msPt: { X: 0, Y: 0 },
        _canvas: null,
        _context: null,
        _drag: false,
        _scale: 1,
        _pan: { X: 0, Y: 0 },

        //
        // options
        //
        options: {
            controller: null,
            color: "white",
            backgroundColor: "black"
        },

        //
        // widget methods
        //
        _create: function() {
            var self = this,
                o = self.options,
                el = self.element,
                cvs = self._canvas = el.get(0),
                ctx = self._context = cvs.getContext('2d'),
                msPt = self._msPt,
                controller = self.options.controller;


            el.addClass("ui-widget").css({
                backgroundColor: o.backgroundColor,
                color: o.color
            });

            el.mousedown(function (event) {
                msPt.X = event.pageX - el.offset().left;
                msPt.Y = event.pageY - el.offset().top;

                if (controller)
                    controller.mousedown(self, self.getWorldPoint(msPt));

                self.draw();
            });

            el.mouseup(function (event) {
                msPt.X = event.pageX - el.offset().left;
                msPt.Y = event.pageY - el.offset().top;

                if (controller)
                    controller.mouseup(self, self.getWorldPoint(msPt));

                self.draw();
            });

            el.mousemove(function(event) {
                var ptNew = {
                    X: event.pageX - el.offset().left,
                    Y: event.pageY - el.offset().top
                };

                if (controller)
                    controller.mousemove(self, self.getWorldPoint(ptNew), self.getWorldPoint(msPt));

                msPt.X = ptNew.X;
                msPt.Y = ptNew.Y;

                self.draw();
            });

            mouseWheelHandler = function(event) {
                if(event.originalEvent.deltaY < 0) {
                  console.log('scroll up');
                } else {
                  console.log('scroll down');
                }

                event.preventDefault();
            };
            el.on('mousewheel', mouseWheelHandler)
              .on('wheel', mouseWheelHandler);
        },
        draw: function() {
            var ctx = this._context,
                msPt = this._msPt,
                model = this.options.model,
                editModes = this.options.editModes,
                curEditMode = this.options.curEditMode;

            ctx.clearRect(0, 0, this.element.get(0).width, this.element.get(0).height);

            ctx.save();
            ctx.transform(this._scale, 0, 0, this._scale, this._pan.x, this._pan.y);

            if (this.options.controller) {
                this.options.controller.draw(this);
            }

            ctx.restore();

            var msPtStr = Math.round(msPt.X / this._scale - this._pan.X) + ", " + Math.round(msPt.Y / this._scale - this._pan.Y);

            this.drawText(msPtStr, 10, 10, 'white');
        },
        destroy: function() {
            this.el.unbind('mousemove');
            this.el.unbind('mousedown')
        },

        //
        // CAD helper functions
        //
        pan: function (dx, dy) {
            this._pan.X += dx;
            this._pan.Y += dy;
        },
        scale: function (scaleVal) {
            this._scale *= scaleVal;
        },
        resetScale: function () {
            this._scale = 1;
        },
        getWorldPoint: function (pt) {
            return {
                X: (pt.X - this._pan.X) / this._scale,
                Y: (pt.Y - this._pan.Y) / this._scale,
            };
        },

        //
        // drawing helper functions
        //
        setCursor: function (cursor) {
            this.element.css('cursor', cursor)
        },
        hitTestPolygon: function (points, testPt) {
            var i, j;
            var c = false;
            for (i = 0, j = points.length - 1; i < points.length; j = i++) {
                if ( ((points[i].Y > testPt.Y) != (points[j].Y > testPt.Y)) &&
                     (testPt.X < (points[j].X - points[i].X) * (testPt.Y - points[i].Y) / (points[j].Y - points[i].Y) + points[i].X) ) {
                    c = !c;
                }
            }
            return c;
        },
        drawText: function (txt, x, y, color) {
            this._context.fillStyle = color;
            this._context.fillText(txt, x, y);
        },
        drawLine: function (ptStart, ptEnd, lineWidth, color) {
            this._context.strokeStyle = color;
            this._context.lineWidth = lineWidth / this._scale;
            this._context.beginPath();
            var pt1 = points[0];
            this._context.moveTo(ptStart.X, ptStart.Y);
            this._context.lineTo(ptEnd.X, ptEnd.Y);
            this._context.closePath();
            this._context.stroke();
        },
        drawPolygon: function (points, lineWidth, color) {
            this._context.strokeStyle = color;
            this._context.lineWidth = lineWidth / this._scale;
            this._context.beginPath();
            var pt1 = points[0];
            this._context.moveTo(pt1.X, pt1.Y);

            for (var ptIdx = 1; ptIdx < points.length; ++ptIdx) {
                var pt = points[ptIdx];
                this._context.lineTo(pt.X, pt.Y);
            }

            this._context.closePath();
            this._context.stroke();
        }
    });

    return $.ui.levcanvas;
}));
