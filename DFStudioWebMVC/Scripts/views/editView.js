var app = app || {};

(function ($) {
    'use strict';

    app.EditView = Backbone.View.extend({

        el: '#dfstudioweb',

        events: {

        },

        initialize: function() {
        },

        loadLevelGeometry: function (levelId) {
            var view = this;
            $.get('/api/Sectors/ByLevelId/' + levelId, function (data) {
                view.initSectors(data);
            });
        },

        initSectors: function (sectors) {
            this.mode = new app.EditModeSector();
            this.mode.lev = {
                sectors: new app.SectorCollection(sectors)
            };
            this.$('#levCanvas').levcanvas({ controller: this.mode });
        }
    });
})(jQuery);
