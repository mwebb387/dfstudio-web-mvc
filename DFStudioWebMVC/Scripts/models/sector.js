/* Defines objects used to manipulate LEV file objects */
var app = app || {};

(function() {
    'use strict';

    app.Sector = Backbone.Model.extend({
       defaults: {
           Name: '',
           Selected: false,
           Vertices: []
       },

       translate: function(x, y) {
           this.get('Vertices').forEach(function(vertex) {
               vertex.X += x;
               vertex.Y += y;
           });
       },
    });

    app.SectorCollection = Backbone.Collection.extend({
        model: app.Sector
    });
})();
