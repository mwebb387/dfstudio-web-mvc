﻿using DFStudioData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace DFStudioWebMVC.Business
{
    public class DFSDatabase
    {
        private static DFSDatabaseContext _Context;
        public static DFSDatabaseContext Context
        {
            get
            {
                if (_Context == null)
                {
                    string dbPath = Path.Combine(
                        HostingEnvironment.ApplicationPhysicalPath,
                        "App_Data",
                        "dfstudio.s3db"
                    );
                    _Context = new DFSDatabaseContext(dbPath);
                }

                return _Context;
            }

        }

        private DFSDatabase()
        { }
    }
}